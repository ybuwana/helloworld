//
//  YBZViewController.h
//  HelloWorld
//
//  Created by Yuki Buwana on 9/14/14.
//  Copyright (c) 2014 Yuki Buwana. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YBZViewController : UIViewController
{
    IBOutlet UILabel *labelHello;
}

- (IBAction)pressMe:(id)sender;

@end
