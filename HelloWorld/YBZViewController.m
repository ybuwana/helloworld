//
//  YBZViewController.m
//  HelloWorld
//
//  Created by Yuki Buwana on 9/14/14.
//  Copyright (c) 2014 Yuki Buwana. All rights reserved.
//

#import "YBZViewController.h"

@interface YBZViewController ()

@end

@implementation YBZViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)pressMe:(id)sender
{
    if ([labelHello.text isEqualToString:@"Hello World"]) {
        [labelHello setText:@"Hello Dunia"];
    } else {
        [labelHello setText:@"Hello World"];
    }
}

@end
